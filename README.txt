********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Deliver Module
Author: Henrique Recidive & Robert Castelo
Drupal: 6.x
********************************************************************
DESCRIPTION:

Email delivering API.


********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.  If you run into
problems, you should always read the INSTALL.txt that comes with the
Drupal package and read the online documentation.

1. Place the entire directory of this module into your Drupal directory:    sites/all/modules/2. Enable the module by navigating to:    administer > build > modulesClick the 'Save configuration' button at the bottom to commit yourchanges. 


********************************************************************
AUTHOR CONTACT

- Report Bugs/Request Features:
   http://drupal.org/project/deliver
   
- Comission New Features:
  http://www.codepositive.com/contact
   

        
********************************************************************
ACKNOWLEDGEMENT

Developed by Henrique Recidive & Robert Castelo for Code Positive <http://www.codepositive.com>




<?php

/**
 * @todo move email settings from subscribed module. 
 */

/**
 * Implementation of hook_help().
 */
function deliver_help($path, $arg) {
  switch ($path) {
    case 'admin/help#deliver' :
      return t('Component: Email delivering API.');
  }
}

/**
 * Implementation of hook_menu().
 */
function deliver_menu() {
  $items['admin/settings/deliver'] = array(
    'title' => 'Email delivering',
    'description' => 'Email delivering settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('deliver_settings'),
    'access arguments' => array('administer site configuration'),
  ); 

  return $items;
}

/**
 * Configure email form.
 */
function deliver_settings() {
  // Email Addresses.
  $form['addresses'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email Addresses'),
    '#description' => t('Make sure that these addresses correspond to mailboxes you have set up on your server.'),
    '#weight' => -10,
  );
  $form['addresses']['from_address'] = array(
    '#type' => 'textfield',
    '#title' => t('From'),
    '#default_value' => variable_get('deliver_from_address', ''),
    '#size' => 40,
    '#maxlength' => 64,
    '#description' => t('The email address you would like messages to be sent from.'),
  );
  $form['addresses']['reply_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Reply-To'),
    '#default_value' => variable_get('deliver_reply_address', ''),
    '#size' => 40,
    '#maxlength' => 64,
    '#description' => t('The email address you would like messages to go to when a recipient uses their Reply button. (can be the same as the From address)'),
  );

  // Sending Configuration.
  $form['send'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sending'),
    '#weight' => -6,
  );
  $form['send']['send_maximum'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum Emails'),
    '#default_value' => variable_get('deliver_send_maximum', '50'),
    '#size' => 10,
    '#maxlength' => 64,
    '#description' => t('The maximum number of emails to send at each cron run'),
  );
  $form['send']['send_delay'] = array(
    '#type' => 'textfield',
    '#title' => t('Send Delay'),
    '#default_value' => variable_get('deliver_send_delay', '2'),
    '#size' => 10,
    '#maxlength' => 64,
    '#description' => t('Delay in microseconds between each email (avoid chocking mail server)'),
  );
  $form['receiving'] = array(
    '#type' => 'fieldset',
    '#title' => t('Receiving'),
    '#weight' => -3,
  );
  $form['receiving']['set_return_path'] = array(
    '#type' => 'radios',
    '#title' => t('Set Return-Path'),
    '#default_value' => variable_get('deliver_set_return_path', t('Yes')),
    '#options' => array(t('No'), t('Yes')),
    '#description' => t("It's preferable to set the Return-Path, but some systems do not allow Drupal to set this."),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );

  return $form;
}

/**
 * Save email configurations.
 */
function deliver_settings_submit($form, &$form_state) {
  $save = array('from_address', 'reply_address', 'send_maximum', 'send_delay', 'set_return_path');

  foreach ($save as $var) {
    variable_set('deliver_'. $var, $form_state['values'][$var]);
  }

  drupal_set_message(t('Email delivering settings have been updated.'));
}

/**
 * Send email.
 */
function deliver_send_email($mailkey, $to, $subject, $content, $markup, $headers = array()) {
  $reply_address = variable_get('deliver_reply_address', '');
  
  $defaults = array(
    'From' => variable_get('deliver_from_address', ''),
    'Reply-to' => $reply_address,
    'X-Original-To' => $reply_address,
  	'X-MessageId' => (time() + rand(1, 32767)),
  	'X-Subscriber' => $to,
  	'X-Date' => time(),
  );

  if (!empty($return_path)) {
    $defaults['Return-path'] = $reply_address;
  }

  if ($markup == 'html') {
    $defaults['Content-Type'] = 'text/html; charset=UTF-8'; 
  }

  $headers = array_merge($defaults, $headers);
 /**
  * @todo update this to Drupal 6 arcane email registry system 
  */
	$message = array(
		'to' => $to,
		'subject' => $subject,
		'body' => $content,
		'headers' => $headers, 
	);  

  if (drupal_mail_send($message)) {
    $sent = TRUE;    
  }
  else {
    drupal_set_message(t("Some emails could not be sent. See log for more detail."), 'error');
    watchdog('warning', 'Email newsletter: problem sending to %to. %subject not sent.', array('%to' => $to, '%subject' => $subject));
    $sent = FALSE;
  } 

  return $sent;
}

/**
 * Generate email header.
 * TODO: improve that function so other modules can add additional headers.
 * TODO: move bounced headers to a hook_mail_alter implementation on bounced_mail module.
 */
function deliver_email_header($service_id, $markup, $to) {
  $return_path = variable_get('deliver_set_return_path', 0);
  $reply_address = variable_get('deliver_reply_address', '');
  $bounce_address = variable_get('bounce_addresss', $reply_address);
  
  $header .= "From: " . variable_get('deliver_from_address', '');
  $header .= "\n";
  $header .= "Reply-to: " . variable_get('deliver_reply_address', '');
  $header .= "\n";  
  if (!empty($return_path) && !empty($bounce_address)) {
    $header .= "Return-path: ". variable_get('deliver_reply_address', '') ."\n";
  }
  $header .= "X-Original-To: " . $reply_address;
  $header .= "\n";  
  $header .= "X-Mailer: Drupal";
  $header .= "\n";  
  $header .= 'X-Service: '. $service_id;
  $header .= "\n";
  $header .= 'X-MessageId: '. (time() + rand(1, 32767));
  $header .= "\n";
  $header .= "X-Subscriber: " . $to;
  $header .= "\n";
  $header .= "X-Date: " . time();
  $header .= "\n";

  if (!empty($bounce_address)) {
    $header .= "Errors-to: " . variable_get('deliver_reply_address', '');
    $header .= "\n";
  }

  $header .= 'MIME-Version: 1.0';
  $header .= "\n"; 

  if ($markup == "html") {
    $header .= 'Content-Type: text/html; charset=UTF-8; Content-transfer-encoding: 8Bit'; 
  }
  else {
    $header .= 'Content-Type: text/plain; charset=UTF-8; format=flowed Content-transfer-encoding: 8Bit';
  }

  return $header;
}
